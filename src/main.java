import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.Arrays;

public class main {


    public static void main(String args[]) {

        MandelbrotCalculator mandelCalc = new MandelbrotCalculator();
        int xdef = 100;
        int ydef = 90;
        int[][] madelbrotData =
                mandelCalc.calcMandelbrotSet(xdef, ydef,
                        MandelbrotCalculator.INITIAL_MIN_REAL,
                        MandelbrotCalculator.INITIAL_MAX_REAL,
                        MandelbrotCalculator.INITIAL_MIN_IMAGINARY,
                        MandelbrotCalculator.INITIAL_MAX_IMAGINARY,
                        MandelbrotCalculator.INITIAL_MAX_ITERATIONS,
                        MandelbrotCalculator.DEFAULT_RADIUS_SQUARED);

        ArrayList<Integer> x = new ArrayList<Integer>();

        for (int j = 0; j < madelbrotData.length; j++) {

            int[] madelbrotDatum = madelbrotData[j];
            for (int i = 0; i < madelbrotDatum.length; i++) {
                //System.out.println(madelbrotDatum[i]);
                x.add(madelbrotDatum[i]);
            }
        }

        //System.out.println(x);

        String mandelbrotString = Arrays.deepToString(madelbrotData);
        // System.out.println(mandelbrotString);

        //System.out.println(Arrays.deepToString(madelbrotData));


        JFrame displayFrame = new JFrame();
        displayFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        displayFrame.setSize(1200, 700);


        GridLayout grid = new GridLayout(ydef, xdef);
        displayFrame.setLayout(grid);


        JButton[] button = new JButton[xdef * ydef];
        // add JButtons dynamically
        for (int i = 0; i < button.length; i++) {
            button[i] = new JButton(String.valueOf(x.get(i)));
            if (x.get(i)<50) {
                button[i].setBackground(Color.RED);
                button[i].setForeground(Color.RED);

            }
            button[i].setPreferredSize(new Dimension(20,20));
            button[i].setFont(new Font("Arial", Font.PLAIN, 6));
//            button[i].setBackground(Color.RED);
//            button[i].setForeground(Color.RED);
//            if (x.get(i) == 2)
      //      button[i].setBackground(Color.black);
            displayFrame.add(button[i]);
        }
        displayFrame.setVisible(true);


    }
}
