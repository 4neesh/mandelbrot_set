import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

import static java.awt.Color.ORANGE;
import static java.awt.Color.WHITE;

public class PaintPanel extends JPanel {

    public void paint(Graphics g) {
        ArrayList<Integer> x = new ArrayList<>();

        MandelbrotCalculator mandelCalc = new MandelbrotCalculator();
        int xdef = 800;
        int ydef = 800;
        int[][] madelbrotData =
                mandelCalc.calcMandelbrotSet(xdef, ydef,
                        MandelbrotCalculator.INITIAL_MIN_REAL,
                        MandelbrotCalculator.INITIAL_MAX_REAL,
                        MandelbrotCalculator.INITIAL_MIN_IMAGINARY,
                        MandelbrotCalculator.INITIAL_MAX_IMAGINARY,
                        MandelbrotCalculator.INITIAL_MAX_ITERATIONS,
                        MandelbrotCalculator.DEFAULT_RADIUS_SQUARED);


        for (int[] madelbrotDatum : madelbrotData)
            for (int i1 : madelbrotDatum) {
                x.add(i1);
            }
        for (int i = 0; i < xdef; i++) {

            for (int j = 0; j < ydef; j++) {

                g.setColor(Color.BLACK);
                int k = xdef * j + i;

                if (x.get(k) < 50) {
                    Color color = Color.getHSBColor( x.get(k) /(float)MandelbrotCalculator.INITIAL_MAX_ITERATIONS , 1.0f, 1.0f);
                    g.setColor(color);
                    //g.setColor(new Color(0, 0, x.get(k) * 5));
                }

                g.drawRect(i, j, 1, 1);

            }
        }

        if (app.drawRectangle == 1) {
            g.setColor(Color.WHITE);
            g.drawRect(app.preX, app.preY, app.draggedX-app.preX, app.draggedY-app.preY);
            app.drawRectangle = 2;
        }

    }

}