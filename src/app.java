import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;


public class app extends JFrame implements MouseListener, MouseMotionListener {
    static int preX;
    static int preY;
    static int draggedX;
    static int draggedY;

    static int drawRectangle;
    private Popup popup;


    private app() {
        getContentPane().add(new PaintPanel());
        setSize(800, 800);
        setVisible(true);
        addMouseListener(this);
        this.addMouseMotionListener(this);
    }

    public static void main(String[] args) {
        new app();
    }


    @Override
    public void mouseClicked(MouseEvent e) {
    }

    @Override
    public void mousePressed(MouseEvent e) {
        preX = e.getX() - 1;
        preY = e.getY() - 26;
        drawRectangle = 1;
        getContentPane().add(new PaintPanel());
        setVisible(true);


    }


    @Override
    public void mouseReleased(MouseEvent e) {
        popup.hide();


        double mouseStartX = preX;
        double mouseEndX = e.getX();
        double mouseStartY = preY;
        double mouseEndY = e.getY();


        if (preX > e.getX()) {
            double temp = e.getX();
            mouseEndX = preX;
            mouseStartX = temp;
        }

        if (preY > e.getY()) {
            double temp = e.getY();
            mouseEndY = preY;
            mouseStartY = temp;
        }


        double absWidth = MandelbrotCalculator.INITIAL_MAX_REAL - MandelbrotCalculator.INITIAL_MIN_REAL;
        double absHeight = MandelbrotCalculator.INITIAL_MAX_IMAGINARY - MandelbrotCalculator.INITIAL_MIN_IMAGINARY;


        getContentPane().add(new PaintPanel());

        setSize(800, 800);

        setVisible(true);
        MandelbrotCalculator.INITIAL_MIN_REAL = MandelbrotCalculator.INITIAL_MIN_REAL + (absWidth * mouseStartX / 800);

        MandelbrotCalculator.INITIAL_MAX_REAL = MandelbrotCalculator.INITIAL_MAX_REAL - absWidth + (absWidth * mouseEndX / 800);

        MandelbrotCalculator.INITIAL_MIN_IMAGINARY = MandelbrotCalculator.INITIAL_MIN_IMAGINARY + (absHeight * mouseStartY / 800);

        MandelbrotCalculator.INITIAL_MAX_IMAGINARY = MandelbrotCalculator.INITIAL_MAX_IMAGINARY - absHeight + (absHeight * mouseEndY / 800);

        //need a scanner and if statement here to allow user to change iterations each go.
//      MandelbrotCalculator.INITIAL_MAX_ITERATIONS = 50 + Math.log10(4 / absWidth);

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {
    }


    @Override
    public void mouseDragged(MouseEvent e) {
        drawRectangle = 1;
        draggedX = e.getX() - 1;
        draggedY = e.getY() - 26;
        getContentPane().add(new PaintPanel());
        setVisible(true);
        if (popup != null) {
            popup.hide();
        }
        double absWidth = MandelbrotCalculator.INITIAL_MAX_REAL - MandelbrotCalculator.INITIAL_MIN_REAL;
        String realX = String.format("%.6g%n", MandelbrotCalculator.INITIAL_MIN_REAL + (e.getX() * absWidth / 800));
        String realY = String.format("%.6g%n", MandelbrotCalculator.INITIAL_MAX_REAL - (e.getX() * absWidth / 800));

        JLabel text = new JLabel(realX + "," + realY);
        popup = PopupFactory.getSharedInstance().getPopup(e.getComponent(), text, e.getXOnScreen() + 15, e.getYOnScreen());
        popup.show();
    }

    @Override
    public void mouseMoved(MouseEvent e) {

    }
}
